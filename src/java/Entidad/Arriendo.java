
package Entidad;

public class Arriendo {

    private int idArriendo;
    private String Wifi;
    private String TipoArriendo;
    private String Estacionamiento;
    private String Cable;
    private String TipoBaño;
    private String TipoCocina;
    private int NumeroPiezas;
    private String Mascotas;
    private String GastosComunes;
    private int idBusqueda;
    
    public int getIdArriendo() {
        return idArriendo;
    }

    public String getWifi() {
        return Wifi;
    }

    public String getTipoArriendo() {
        return TipoArriendo;
    }

    public String getEstacionamiento() {
        return Estacionamiento;
    }

    public String getCable() {
        return Cable;
    }

    public String getTipoBaño() {
        return TipoBaño;
    }

    public String getTipoCocina() {
        return TipoCocina;
    }

    public int getNumeroPiezas() {
        return NumeroPiezas;
    }

    public String getMascotas() {
        return Mascotas;
    }

    public String getGastosComunes() {
        return GastosComunes;
    }

    public int getIdBusqueda() {
        return idBusqueda;
    }

    public void setIdArriendo(int idArriendo) {
        this.idArriendo = idArriendo;
    }

    public void setWifi(String Wifi) {
        this.Wifi = Wifi;
    }

    public void setTipoArriendo(String TipoArriendo) {
        this.TipoArriendo = TipoArriendo;
    }

    public void setEstacionamiento(String Estacionamiento) {
        this.Estacionamiento = Estacionamiento;
    }

    public void setCable(String Cable) {
        this.Cable = Cable;
    }

    public void setTipoBaño(String TipoBaño) {
        this.TipoBaño = TipoBaño;
    }

    public void setTipoCocina(String TipoCocina) {
        this.TipoCocina = TipoCocina;
    }

    public void setNumeroPiezas(int NumeroPiezas) {
        this.NumeroPiezas = NumeroPiezas;
    }

    public void setMascotas(String Mascotas) {
        this.Mascotas = Mascotas;
    }

    public void setGastosComunes(String GastosComunes) {
        this.GastosComunes = GastosComunes;
    }

    public void setIdBusqueda(int idBusqueda) {
        this.idBusqueda = idBusqueda;
    }

    @Override
    public String toString() {
        return "Arriendo{" + "idArriendo=" + idArriendo + ", Wifi=" + Wifi + ", TipoArriendo=" + TipoArriendo + ", Estacionamiento=" + Estacionamiento + ", Cable=" + Cable + ", TipoBa\u00f1o=" + TipoBaño + ", TipoCocina=" + TipoCocina + ", NumeroPiezas=" + NumeroPiezas + ", Mascotas=" + Mascotas + ", GastosComunes=" + GastosComunes + ", idBusqueda=" + idBusqueda + '}';
    }
    
}
