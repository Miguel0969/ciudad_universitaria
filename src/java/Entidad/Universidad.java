
package Entidad;


public class Universidad {
    
    private int idUniversidad;
    private String ProgramaPractica;
    private String AcuerdosEmpresas;
    private String TipoUniversidad;
    private String Acreditacion;
    private String ProgamaIntercambio;
    private int ActCulturales;
    private int idBusqueda;
    
    public int getIdUniversidad() {
        return idUniversidad;
    }

    public void setIdUniversidad(int idUniversidad) {
        this.idUniversidad = idUniversidad;
    }

    public String getProgramaPractica() {
        return ProgramaPractica;
    }

    public void setProgramaPractica(String ProgramaPractica) {
        this.ProgramaPractica = ProgramaPractica;
    }

    public String getAcuerdosEmpresas() {
        return AcuerdosEmpresas;
    }

    public void setAcuerdosEmpresas(String AcuerdosEmpresas) {
        this.AcuerdosEmpresas = AcuerdosEmpresas;
    }

    public String getTipoUniversidad() {
        return TipoUniversidad;
    }

    public void setTipoUniversidad(String TipoUniversidad) {
        this.TipoUniversidad = TipoUniversidad;
    }

    public String getAcreditacion() {
        return Acreditacion;
    }

    public void setAcreditacion(String Acreditacion) {
        this.Acreditacion = Acreditacion;
    }

    public String getProgamaIntercambio() {
        return ProgamaIntercambio;
    }

    public void setProgamaIntercambio(String ProgamaIntercambio) {
        this.ProgamaIntercambio = ProgamaIntercambio;
    }

    public int getActCulturales() {
        return ActCulturales;
    }

    public void setActCulturales(int ActCulturales) {
        this.ActCulturales = ActCulturales;
    }

    public int getIdBusqueda() {
        return idBusqueda;
    }

    public void setIdBusqueda(int idBusqueda) {
        this.idBusqueda = idBusqueda;
    }

    @Override
    public String toString() {
        return "Universidad{" + "idUniversidad=" + idUniversidad + ", ProgramaPractica=" + ProgramaPractica + ", AcuerdosEmpresas=" + AcuerdosEmpresas + ", TipoUniversidad=" + TipoUniversidad + ", Acreditacion=" + Acreditacion + ", ProgamaIntercambio=" + ProgamaIntercambio + ", ActCulturales=" + ActCulturales + ", idBusqueda=" + idBusqueda + '}';
    }
    
}
