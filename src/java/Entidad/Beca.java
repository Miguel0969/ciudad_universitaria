
package Entidad;

public class Beca {

    private int idBeca;
    private String NombreBeca;
    private String Descripcion;
    
    
    public int getIdBeca() {
        return idBeca;
    }

    public void setIdBeca(int idBeca) {
        this.idBeca = idBeca;
    }

    public String getNombreBeca() {
        return NombreBeca;
    }

    public void setNombreBeca(String NombreBeca) {
        this.NombreBeca = NombreBeca;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    @Override
    public String toString() {
        return "Beca{" + "idBeca=" + idBeca + ", NombreBeca=" + NombreBeca + ", Descripcion=" + Descripcion + '}';
    }
 
    
    
}
