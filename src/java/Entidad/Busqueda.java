
package Entidad;

public class Busqueda {

    private int idBusqueda;
    private String UbicacionBusqueda;
    private String DscUbicacion;
    private String DscBusqueda;
    private String NombreBusqueda;
    private String TipoBusqueda;
    private String NotaBusqueda;
    private String PaginaWeb;
    private int Precio;
    private int Telefono;
    
    public int getIdBusqueda() {
        return idBusqueda;
    }

    public void setIdBusqueda(int idBusqueda) {
        this.idBusqueda = idBusqueda;
    }

    public String getUbicacionBusqueda() {
        return UbicacionBusqueda;
    }

    public void setUbicacionBusqueda(String UbicacionBusqueda) {
        this.UbicacionBusqueda = UbicacionBusqueda;
    }

    public String getDscUbicacion() {
        return DscUbicacion;
    }

    public void setDscUbicacion(String DscUbicacion) {
        this.DscUbicacion = DscUbicacion;
    }

    public String getDscBusqueda() {
        return DscBusqueda;
    }

    public void setDscBusqueda(String DscBusqueda) {
        this.DscBusqueda = DscBusqueda;
    }

    public String getNombreBusqueda() {
        return NombreBusqueda;
    }

    public void setNombreBusqueda(String NombreBusqueda) {
        this.NombreBusqueda = NombreBusqueda;
    }

    public String getTipoBusqueda() {
        return TipoBusqueda;
    }

    public void setTipoBusqueda(String TipoBusqueda) {
        this.TipoBusqueda = TipoBusqueda;
    }

    public String getNotaBusqueda() {
        return NotaBusqueda;
    }

    public void setNotaBusqueda(String NotaBusqueda) {
        this.NotaBusqueda = NotaBusqueda;
    }

    public String getPaginaWeb() {
        return PaginaWeb;
    }

    public void setPaginaWeb(String PaginaWeb) {
        this.PaginaWeb = PaginaWeb;
    }

    public int getPrecio() {
        return Precio;
    }

    public void setPrecio(int Precio) {
        this.Precio = Precio;
    }

    public int getTelefono() {
        return Telefono;
    }

    public void setTelefono(int Telefono) {
        this.Telefono = Telefono;
    }

    @Override
    public String toString() {
        return "Busqueda{" + "idBusqueda=" + idBusqueda + ", UbicacionBusqueda=" + UbicacionBusqueda + ", DscUbicacion=" + DscUbicacion + ", DscBusqueda=" + DscBusqueda + ", NombreBusqueda=" + NombreBusqueda + ", TipoBusqueda=" + TipoBusqueda + ", NotaBusqueda=" + NotaBusqueda + ", PaginaWeb=" + PaginaWeb + ", Precio=" + Precio + ", Telefono=" + Telefono + '}';
    }
    
    
    
}
