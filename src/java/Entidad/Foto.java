
package Entidad;


public class Foto {
    
    private int idFoto;
    private byte Foto;
    private String TipoFoto;
    private int idBusqueda;
    
    public int getIdFoto() {
        return idFoto;
    }

    public void setIdFoto(int idFoto) {
        this.idFoto = idFoto;
    }

    public byte getFoto() {
        return Foto;
    }

    public void setFoto(byte Foto) {
        this.Foto = Foto;
    }

    public String getTipoFoto() {
        return TipoFoto;
    }

    public void setTipoFoto(String TipoFoto) {
        this.TipoFoto = TipoFoto;
    }

    public int getIdBusqueda() {
        return idBusqueda;
    }

    public void setIdBusqueda(int idBusqueda) {
        this.idBusqueda = idBusqueda;
    }

    @Override
    public String toString() {
        return "Foto{" + "idFoto=" + idFoto + ", Foto=" + Foto + ", TipoFoto=" + TipoFoto + ", idBusqueda=" + idBusqueda + '}';
    }
    
}

