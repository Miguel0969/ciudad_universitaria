
package Entidad;


public class Evento {
    
    private int idBusqueda;
    private int idUniversidad;
    private String TipoEvento;
    
    public int getIdBusqueda() {
        return idBusqueda;
    }

    public void setIdBusqueda(int idBusqueda) {
        this.idBusqueda = idBusqueda;
    }

    public int getIdUniversidad() {
        return idUniversidad;
    }

    public void setIdUniversidad(int idUniversidad) {
        this.idUniversidad = idUniversidad;
    }

    public String getTipoEvento() {
        return TipoEvento;
    }

    public void setTipoEvento(String TipoEvento) {
        this.TipoEvento = TipoEvento;
    }

    @Override
    public String toString() {
        return "Evento{" + "idBusqueda=" + idBusqueda + ", idUniversidad=" + idUniversidad + ", TipoEvento=" + TipoEvento + '}';
    }
    
}
