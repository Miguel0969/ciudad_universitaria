
package Entidad;


public class Detalle_Carrera {
    
    private int idBusqueda;
    private int idCarrera;
    private byte Malla;
    private String ReqPsu;
    
    public int getIdBusqueda() {
        return idBusqueda;
    }

    public void setIdBusqueda(int idBusqueda) {
        this.idBusqueda = idBusqueda;
    }

    public int getIdCarrera() {
        return idCarrera;
    }

    public void setIdCarrera(int idCarrera) {
        this.idCarrera = idCarrera;
    }

    public byte getMalla() {
        return Malla;
    }

    public void setMalla(byte Malla) {
        this.Malla = Malla;
    }

    public String getReqPsu() {
        return ReqPsu;
    }

    public void setReqPsu(String ReqPsu) {
        this.ReqPsu = ReqPsu;
    }

    @Override
    public String toString() {
        return "Detalle_Carrera{" + "idBusqueda=" + idBusqueda + ", idCarrera=" + idCarrera + ", Malla=" + Malla + ", ReqPsu=" + ReqPsu + '}';
    }
    
      
    
}
