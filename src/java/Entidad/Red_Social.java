
package Entidad;


public class Red_Social {
    
    private int idRedSocial;
    private String NombreRedSocial;
    
    public int getIdRedSocial() {
        return idRedSocial;
    }

    public void setIdRedSocial(int idRedSocial) {
        this.idRedSocial = idRedSocial;
    }

    public String getNombreRedSocial() {
        return NombreRedSocial;
    }

    public void setNombreRedSocial(String NombreRedSocial) {
        this.NombreRedSocial = NombreRedSocial;
    }

    @Override
    public String toString() {
        return "Red_Social{" + "idRedSocial=" + idRedSocial + ", NombreRedSocial=" + NombreRedSocial + '}';
    }
    
    
}
