
package Entidad;

public class Detalle_Jornada {

    private int idCarrera;
    private int idJornada;
    
    public int getIdCarrera() {
        return idCarrera;
    }

    public void setIdCarrera(int idCarrera) {
        this.idCarrera = idCarrera;
    }

    public int getIdJornada() {
        return idJornada;
    }

    public void setIdJornada(int idJornada) {
        this.idJornada = idJornada;
    }

    @Override
    public String toString() {
        return "Detalle_Jornada{" + "idCarrera=" + idCarrera + ", idJornada=" + idJornada + '}';
    }
    
}
