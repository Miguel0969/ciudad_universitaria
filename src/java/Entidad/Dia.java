
package Entidad;


public class Dia {
    
    private int idDia;
    private String NombreDia;
    
    public int getIdDia() {
        return idDia;
    }

    public void setIdDia(int idDia) {
        this.idDia = idDia;
    }

    public String getNombreDia() {
        return NombreDia;
    }

    public void setNombreDia(String NombreDia) {
        this.NombreDia = NombreDia;
    }

    @Override
    public String toString() {
        return "Dia{" + "idDia=" + idDia + ", NombreDia=" + NombreDia + '}';
    }
    
    
}
