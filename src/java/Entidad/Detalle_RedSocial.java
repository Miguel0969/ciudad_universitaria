
package Entidad;


public class Detalle_RedSocial {
    
    private int idBusqueda;
    private int idRedSocial;
    private String link;
    
    public int getIdBusqueda() {
        return idBusqueda;
    }

    public void setIdBusqueda(int idBusqueda) {
        this.idBusqueda = idBusqueda;
    }

    public int getIdRedSocial() {
        return idRedSocial;
    }

    public void setIdRedSocial(int idRedSocial) {
        this.idRedSocial = idRedSocial;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public String toString() {
        return "Detalle_RedSocial{" + "idBusqueda=" + idBusqueda + ", idRedSocial=" + idRedSocial + ", link=" + link + '}';
    }
    
    
    
}


