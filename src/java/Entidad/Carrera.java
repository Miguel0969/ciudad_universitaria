
package Entidad;

public class Carrera {

    private int idCarrera;
    private String TipoCarrera;
    private byte MallaCurricular;
    private String Horario;
    private String DetalleCarrera;
    private String Facultad;
    
    
    public int getIdCarrera() {
        return idCarrera;
    }

    public void setIdCarrera(int idCarrera) {
        this.idCarrera = idCarrera;
    }

    public String getTipoCarrera() {
        return TipoCarrera;
    }

    public void setTipoCarrera(String TipoCarrera) {
        this.TipoCarrera = TipoCarrera;
    }

    public byte getMallaCurricular() {
        return MallaCurricular;
    }

    public void setMallaCurricular(byte MallaCurricular) {
        this.MallaCurricular = MallaCurricular;
    }

    public String getHorario() {
        return Horario;
    }

    public void setHorario(String Horario) {
        this.Horario = Horario;
    }

    public String getDetalleCarrera() {
        return DetalleCarrera;
    }

    public void setDetalleCarrera(String DetalleCarrera) {
        this.DetalleCarrera = DetalleCarrera;
    }

    public String getFacultad() {
        return Facultad;
    }

    public void setFacultad(String Facultad) {
        this.Facultad = Facultad;
    }

    @Override
    public String toString() {
        return "Carrera{" + "idCarrera=" + idCarrera + ", TipoCarrera=" + TipoCarrera + ", MallaCurricular=" + MallaCurricular + ", Horario=" + Horario + ", DetalleCarrera=" + DetalleCarrera + ", Facultad=" + Facultad + '}';
    }

    
    
}
