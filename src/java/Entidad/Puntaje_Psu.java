
package Entidad;


public class Puntaje_Psu {
    
    private int idPuntaje;
    private int idBusqueda;
    private int idCarrera;
    private int Puntaje;
    private String Materia;
    
    public int getIdPuntaje() {
        return idPuntaje;
    }

    public void setIdPuntaje(int idPuntaje) {
        this.idPuntaje = idPuntaje;
    }

    public int getIdBusqueda() {
        return idBusqueda;
    }

    public void setIdBusqueda(int idBusqueda) {
        this.idBusqueda = idBusqueda;
    }

    public int getIdCarrera() {
        return idCarrera;
    }

    public void setIdCarrera(int idCarrera) {
        this.idCarrera = idCarrera;
    }

    public int getPuntaje() {
        return Puntaje;
    }

    public void setPuntaje(int Puntaje) {
        this.Puntaje = Puntaje;
    }

    public String getMateria() {
        return Materia;
    }

    public void setMateria(String Materia) {
        this.Materia = Materia;
    }

    @Override
    public String toString() {
        return "Puntaje_Psu{" + "idPuntaje=" + idPuntaje + ", idBusqueda=" + idBusqueda + ", idCarrera=" + idCarrera + ", Puntaje=" + Puntaje + ", Materia=" + Materia + '}';
    }
    
    
}
