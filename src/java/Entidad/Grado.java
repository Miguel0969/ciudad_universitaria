
package Entidad;

public class Grado {
    
    private int idGrado;
    private String NombreGrado;
    
    public int getIdGrado() {
        return idGrado;
    }

    public void setIdGrado(int idGrado) {
        this.idGrado = idGrado;
    }

    public String getNombreGrado() {
        return NombreGrado;
    }

    public void setNombreGrado(String NombreGrado) {
        this.NombreGrado = NombreGrado;
    }

    @Override
    public String toString() {
        return "Grado{" + "idGrado=" + idGrado + ", NombreGrado=" + NombreGrado + '}';
    }
    
    
}
