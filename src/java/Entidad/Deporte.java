
package Entidad;


public class Deporte {
    
    private int idDeporte;
    private String nombreDeporte;
    private String Sexo;
    
    public int getIdDeporte() {
        return idDeporte;
    }

    public void setIdDeporte(int idDeporte) {
        this.idDeporte = idDeporte;
    }

    public String getNombreDeporte() {
        return nombreDeporte;
    }

    public void setNombreDeporte(String nombreDeporte) {
        this.nombreDeporte = nombreDeporte;
    }

    public String getSexo() {
        return Sexo;
    }

    public void setSexo(String Sexo) {
        this.Sexo = Sexo;
    }

    @Override
    public String toString() {
        return "Deporte{" + "idDeporte=" + idDeporte + ", nombreDeporte=" + nombreDeporte + ", Sexo=" + Sexo + '}';
    }
    
    
    
    
}
