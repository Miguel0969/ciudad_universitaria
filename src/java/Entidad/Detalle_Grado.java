
package Entidad;


public class Detalle_Grado {
    
    private int idCarrera;
    private int idGrado;
    
    public int getIdCarrera() {
        return idCarrera;
    }

    public void setIdCarrera(int idCarrera) {
        this.idCarrera = idCarrera;
    }

    public int getIdGrado() {
        return idGrado;
    }

    public void setIdGrado(int idGrado) {
        this.idGrado = idGrado;
    }

    @Override
    public String toString() {
        return "Detalle_Grado{" + "idCarrera=" + idCarrera + ", idGrado=" + idGrado + '}';
    }
    
    
    
}
