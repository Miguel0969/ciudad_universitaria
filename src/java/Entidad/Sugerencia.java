
package Entidad;


public class Sugerencia {
    
    private int idSugerencia;
    private String Asunto;
    private String DscSugerencia;
    private int idUsuario;
    
    public int getIdSugerencia() {
        return idSugerencia;
    }

    public void setIdSugerencia(int idSugerencia) {
        this.idSugerencia = idSugerencia;
    }

    public String getAsunto() {
        return Asunto;
    }

    public void setAsunto(String Asunto) {
        this.Asunto = Asunto;
    }

    public String getDscSugerencia() {
        return DscSugerencia;
    }

    public void setDscSugerencia(String DscSugerencia) {
        this.DscSugerencia = DscSugerencia;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public String toString() {
        return "Sugerencia{" + "idSugerencia=" + idSugerencia + ", Asunto=" + Asunto + ", DscSugerencia=" + DscSugerencia + ", idUsuario=" + idUsuario + '}';
    }
    
}
