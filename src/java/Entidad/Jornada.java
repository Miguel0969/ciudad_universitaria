
package Entidad;


public class Jornada {
    
    private int idJornada;
    private String NombreJornada;
    
    public int getIdJornada() {
        return idJornada;
    }

    public void setIdJornada(int idJornada) {
        this.idJornada = idJornada;
    }

    public String getNombreJornada() {
        return NombreJornada;
    }

    public void setNombreJornada(String NombreJornada) {
        this.NombreJornada = NombreJornada;
    }

    @Override
    public String toString() {
        return "Jornada{" + "idJornada=" + idJornada + ", NombreJornada=" + NombreJornada + '}';
    }
    
    
}
