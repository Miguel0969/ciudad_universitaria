
package Entidad;

import java.sql.Date;


public class Detalle_HrAtencion {
    
    private int idDia;
    private int idBusqueda;
    private Date HoraApertura;
    private Date HoraCierre;
    
    public int getIdDia() {
        return idDia;
    }

    public void setIdDia(int idDia) {
        this.idDia = idDia;
    }

    public int getIdBusqueda() {
        return idBusqueda;
    }

    public void setIdBusqueda(int idBusqueda) {
        this.idBusqueda = idBusqueda;
    }

    public Date getHoraApertura() {
        return HoraApertura;
    }

    public void setHoraApertura(Date HoraApertura) {
        this.HoraApertura = HoraApertura;
    }

    public Date getHoraCierre() {
        return HoraCierre;
    }

    public void setHoraCierre(Date HoraCierre) {
        this.HoraCierre = HoraCierre;
    }

    @Override
    public String toString() {
        return "Detalle_HrAtencion{" + "idDia=" + idDia + ", idBusqueda=" + idBusqueda + ", HoraApertura=" + HoraApertura + ", HoraCierre=" + HoraCierre + '}';
    }
    
    
    
}
