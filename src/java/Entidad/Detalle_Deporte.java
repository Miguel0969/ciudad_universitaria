
package Entidad;


public class Detalle_Deporte {

    private int idUniversidad;
    private int idDeporte;
    
    public int getIdUniversidad() {
        return idUniversidad;
    }

    public void setIdUniversidad(int idUniversidad) {
        this.idUniversidad = idUniversidad;
    }

    public int getIdDeporte() {
        return idDeporte;
    }

    public void setIdDeporte(int idDeporte) {
        this.idDeporte = idDeporte;
    }

    @Override
    public String toString() {
        return "Detalle_Deporte{" + "idUniversidad=" + idUniversidad + ", idDeporte=" + idDeporte + '}';
    }
    
    
    
}
