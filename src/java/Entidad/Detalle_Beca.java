
package Entidad;


public class Detalle_Beca {

    private int idBusqueda;
    private int idBeca;
    
    public int getIdBusqueda() {
        return idBusqueda;
    }

    public void setIdBusqueda(int idBusqueda) {
        this.idBusqueda = idBusqueda;
    }

    public int getIdBeca() {
        return idBeca;
    }

    public void setIdBeca(int idBeca) {
        this.idBeca = idBeca;
    }

    @Override
    public String toString() {
        return "Detalle_Beca{" + "idBusqueda=" + idBusqueda + ", idBeca=" + idBeca + '}';
    }
    
}
