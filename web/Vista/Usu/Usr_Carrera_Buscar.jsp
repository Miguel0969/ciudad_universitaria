<%-- 
    Document   : Usr_Carrera_Buscar
    Created on : 11-oct-2019, 18:27:10
    Author     : Casa23
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
	<title>Buscar carreras</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons.js"></script>
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    </head>
    <body class="bg-dark">
	<!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera -->
	<div class="container-fluid bg-dark">
  		<div class="row">
  			<div id="header" class="col-md-7" style=" margin-top: 20px;">
  				<center>
  					<a id="aCiuUni" class="navbar-brand text-white" href="#">Ciudad universitaria <br> Puerto Montt</a>
  				</center>
  			</div>
  			<div id="header" class="col-md-2 d-flex flex-row-reverse">
  				<h6 class="text-white">Nombre de usuario</h6>
  			</div>
  			<div class="col-md-3 d-flex flex-row">
  				<img style="height: 65px;width: 60px; margin-top: 10px;" src="../icons/estudiante.png">			
				<a href="#" id="btnCerrarSesion" class="btn btn-transparent"><img src="https://img.icons8.com/officel/26/000000/circled-right-2.png">Cerrar sesion</a>
  			</div>
		</div>
	</div>
	<!-- Cabecera2 --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera2 -->
	<div class="container-fluid bg-light">
  		<div class="row border border-dark rounded-0 border-right-0 border-left-0">
  			<div class="col-md-12" style="margin-top: 10px;">
  				<ul class="nav justify-content-center">
  					<li class="nav-item">
  						<button class="btn btn-light rounded-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Inicio
						  </button>
  					</li>
				  	<li class="nav-item">
				    	<div class="dropdown">
						  <button class="btn btn-light rounded-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Universidades
						  </button>
						  <div class="dropdown-menu border border-secondary" aria-labelledby="dropdownMenu2">
								<center>
							    	<a class="btn btn-light" href="#" style="width:100%;">Buscar</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Ver todas</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Comparar</a>
						    	</center>
						  </div>
						</div>
				  	</li>
				 	<li class="nav-item">
				    	<div class="dropdown">
						  <button class="btn btn-light rounded-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Arriendos
						  </button>
						  <div class="dropdown-menu border border-secondary" aria-labelledby="dropdownMenu2">
								<center>
							    	<a class="btn btn-light" href="#" style="width:100%;">Buscar</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Ver todos</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Comparar</a>
						    	</center>
						  </div>
						</div>
				  	</li>
				  	<li class="nav-item">
				    	<div class="dropdown">
						  <button class="btn btn-light rounded-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Carreras
						  </button>
						  <div class="dropdown-menu border border-secondary" aria-labelledby="dropdownMenu2">
								<center>
							    	<a class="btn btn-light" href="#" style="width:100%;">Buscar</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Ver todas</a>
						    	</center>
						  </div>
						</div>
				  	</li>
				  	<li class="nav-item">
				    	<div class="dropdown">
						  <button class="btn btn-light rounded-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Lugares de entretencion
						  </button>
						  <div class="dropdown-menu border border-secondary" aria-labelledby="dropdownMenu2">
								<center>
							    	<a class="btn btn-light" href="#" style="width:100%;">Buscar</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Ver todas</a>
						    	</center>
						  </div>
						</div>
				  	</li>
				  	<li class="nav-item">
				    	<div class="dropdown">
						  <button class="btn btn-light rounded-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Becas
						  </button>
						  <div class="dropdown-menu border border-secondary" aria-labelledby="dropdownMenu2">
								<center>
							    	<a class="btn btn-light" href="#" style="width:100%;">Buscar</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Ver todas</a>
						    	</center>
						  </div>
						</div>
				  	</li>
				  	<li class="nav-item">
				    	<div class="dropdown">
						  <button class="btn btn-light rounded-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Eventos
						  </button>
						  <div class="dropdown-menu border border-secondary" aria-labelledby="dropdownMenu2">
								<center>
							    	<a class="btn btn-light" href="#" style="width:100%;">Buscar</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Ver todos</a>
						    	</center>
						  </div>
						</div>
				  	</li>
				</ul>
  			</div>
		</div>
	</div>
	<!-- Cuerpo1 --><!-- Cuerpo1 --><!-- Cuerpo1 --><!-- Cuerpo1 --><!-- Cuerpo1 --><!-- Cuerpo1 --><!-- Cuerpo1 --><!-- Cuerpo1 --><!-- Cuerpo1 --><!-- Cuerpo1 --><!-- Cuerpo1 --><!-- Cuerpo1 --><!-- Cuerpo1 --><!-- Cuerpo1 --><!-- Cuerpo1 --><!-- Cuerpo1 -->
	<div class="container bg-light">
		<div class="row">
			<div class="col-md-12">
				<br><br>
				<center>
					<h2>Búsqueda de carreras</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam magnam, veritatis a enim dicta mollitia praesentium.</p>
				</center>
				<br>
			</div>
		</div>
		<div class="row" id="casiFootArriendos">
			<div class="col-md-2" id="PaginacionArriendo1">
				<center>
					<h6>Carrera</h6>
					<input class="form-control" type="text" placeholder="Carrera">
				</center>
			</div>
			<div class="form-group col-md-2">
		      <label for="inputState">Universidad</label>
		      <select id="inputState" class="form-control">
		        <option selected>Seleccione...</option>
		        <option>xxxxx</option>
		        <option>xxxxx</option>
		        <option>xxxxx</option>
		      </select>
		    </div>
			<div class="form-group col-md-2">
		      <label for="inputState">Jornada</label>
		      <select id="inputState" class="form-control">
		        <option selected>Seleccione...</option>
		        <option>xxxxx</option>
		        <option>xxxxx</option>
		        <option>xxxxx</option>
		      </select>
		    </div>
			<div class="form-group col-md-2"  id="PaginacionArriendo2">
		      <label for="inputState">Facultad</label>
		      <select id="inputState" class="form-control">
		        <option selected>Seleccione...</option>
		        <option>xxxxx</option>
		        <option>xxxxx</option>
		        <option>xxxxx</option>
		      </select>
		    </div>
		</div>
		<div class="row">
			<div class="col-md-8" id="casiFootArriendos">
				<br>
				<center>
					<a href="" class="btn btn-light border">Buscar carrera</a>
				</center>
				<br>
			</div>
		</div>
	</div>
	<!-- Cuerpo1 --><!-- Cuerpo1 --><!-- Cuerpo1 --><!-- Cuerpo1 --><!-- Cuerpo1 --><!-- Cuerpo1 --><!-- Cuerpo1 --><!-- Cuerpo1 --><!-- Cuerpo1 --><!-- Cuerpo1 --><!-- Cuerpo1 --><!-- Cuerpo1 --><!-- Cuerpo1 --><!-- Cuerpo1 --><!-- Cuerpo1 --><!-- Cuerpo1 -->
	<div class="container-fluid bg-light border border-dark">
		<div class="row">
			<div class="col-md-12">
				<center>
					<br>
					<h2>Resultados de busqueda</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, veritatis porro nulla.</p>
					<br>
					<br>
				</center>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6" id="crdBuscCarre">
				<div class="card bg-dark text-white">
				  <img id="bscCarrera" src="img/BuscarCarrera/Carrera1.jpg" class="card-img" alt="...">
				  <div class="card-img-overlay">
				    <center>
					    <h5 id="crdTituloBuscCarre" class="card-title border border-dark border-bottom-0 rounded-0" style="margin-top: 35%;">Carrera 1</h5>
					    <p id="crdTituloBuscCarre" class="card-text border border-dark border-top-0 rounded-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae ex natus facilis soluta, dolorum nisi unde. Accusamus vitae, iure ea, nobis similique, tenetur dolor maiores, ut quos quod quidem sunt!</p>
				    	<a href="#" class="btn btn-dark">Ver mas</a>
				    </center>
				  </div>
				</div>
				<br>
			</div>
			<div class="col-md-6" id="crdBuscCarre">
				<div class="card bg-dark text-white">
				  <img id="bscCarrera" src="img/BuscarCarrera/Carrera2.jpg" class="card-img" alt="...">
				  <div class="card-img-overlay">
				  	<center>
					    <h5 id="crdTituloBuscCarre" class="card-title border border-dark border-bottom-0 rounded-0" style="margin-top: 35%;">Carrera 2</h5>
					    <p id="crdTituloBuscCarre" class="card-text border border-dark border-top-0 rounded-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga a, excepturi et aliquid, praesentium ad. Tenetur corrupti quae id excepturi amet sint, harum numquam corporis, nobis voluptas ratione similique rem.</p>
					    <a href="#" class="btn btn-dark">Ver mas</a>
				    </center>
				  </div>
				</div>
				<br>
			</div>
		</div>
	</div>

	<!-- FOOOTer2222 --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer -->
	<!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer -->
	<div class="container-fluid bg-dark">
  		<div class="row">
  			<div class="col-md-2"></div>
  			<div class="col-md-8" id="casiFootArriendos">
	  			<center>
		  			<h3 class="text-white border border-white border-left-0 border-right-0 border-bottom-0" style="margin-top: 40px;">¿Te gusto la pagina?</h3>
		  			<h6 class="text-white">¡Compartela con tus amigos!</h6>
				<div class="btn-group dropright">
				  <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				 	<img src="https://img.icons8.com/bubbles/50/000000/share.png">
				  </button>
				  <div class="dropdown-menu">
				    <a class="btn btn-light" href="#"><img src="https://img.icons8.com/bubbles/50/000000/facebook-new.png"></a>
							    <a class="btn btn-light" href="#"><img src="https://img.icons8.com/bubbles/50/000000/twitter.png"></a>
							    <a class="btn btn-light" href="#"><img src="https://img.icons8.com/bubbles/50/000000/google-plus.png"></a>
							    <a class="btn btn-light" href="#"><img src="https://img.icons8.com/bubbles/50/000000/instagram-new.png"></a>
							    <a class="btn btn-light" href="#"><img style="width: 50px;height: 50px;" src="https://img.icons8.com/dusk/64/000000/ms-outlook.png"></a>
				  </div>
				</div>
				<h6 class="text-white border border-white border-left-0 border-right-0 border-top-0" style="margin-bottom: 20px;">Compartir</h6>
				</center>
  			</div>
  			<div class="col-md-2"></div>
		</div>
		<center><p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit..</p>
		<li class="nav-item" style="margin-top: -35px;">
			<div class="dropdown">
				<button class="btn btn-transparent text-white" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Icons by
				</button>
				<div class="dropdown-menu" aria-labelledby="dropdownMenu2">
					<a class="btn btn-light" href="https://icons8.com/icon/118555/facebook-nuevo">Facebook Nuevo icon by Icons8</a>
					<a class="btn btn-light" href="https://icons8.com/icon/114209/google-plus">Google Plus icon by Icons8</a>
					<a class="btn btn-light" href="https://icons8.com/icon/108650/twitter">Twitter icon by Icons8></a>
					<a class="btn btn-light" href="https://icons8.com/icon/46977/ms-outlook">MS Outlook icon by Icons8</a>
					<a class="btn btn-light" href="https://icons8.com/icon/108646/instagram">Instagram icon by Icons8</a>
					<a class="btn btn-light" href="https://icons8.com/icon/109462/share">Share icon by Icons8</a>
					<a href="https://www.flaticon.es/autores/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.es/"             title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/"             title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a>
					<a href="https://icons8.com/icon/4N5w6YH6O7Ub/derecha-en-círculo-2">Derecha en círculo 2 icon by Icons8</a>
				</div>
			</div>
		</li>
		</center>
	</div>

	<!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
