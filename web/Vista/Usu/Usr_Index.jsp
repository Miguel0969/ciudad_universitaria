<%-- 
    Document   : Usr_Index
    Created on : 11-oct-2019, 13:41:43
    Author     : Casa23
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
	<title>Ciudad Universitaria</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons.js"></script>
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
        <script src="http://code.jquery.com/jquery-latest.js"></script>
    </head>

    <body class="bg-dark">
  	<div class="container-fluid bg-dark">
  		<div class="row">
  			<div id="header" class="col-md-6" style=" margin-top: 20px;">
  				<center>
  					<a id="aCiuUni" class="navbar-brand text-white" href="#">Ciudad universitaria <br> Puerto Montt</a>
  				</center>
  			</div>
  			<div id="header" class="col-md-3 d-flex flex-row-reverse">
  				<a class="btn btn-dark border rounded-0 text-white" style="margin-bottom: 10px;" href="#">Iniciar sesion</a>
  			</div>
  			<div id="header" class="col-md-3 d-flex flex-row">
  				<a class="btn btn-dark border rounded-0 text-white" style="margin-bottom: 10px;" href="#">Registrarse</a>
  			</div>
		</div>
	</div>
	<div class="container-fluid bg-light">
  		<div class="row border border-dark rounded-0 border-right-0 border-left-0">
  			<div class="col-md-12" style="margin-top: 10px;">
  				<ul class="nav justify-content-center">
  					<li class="nav-item">
  						<button class="btn btn-light rounded-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Inicio
						  </button>
  					</li>
				  	<li class="nav-item">
				    	<div class="dropdown">
						  <button class="btn btn-light rounded-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Universidades
						  </button>
						  <div class="dropdown-menu border border-secondary" aria-labelledby="dropdownMenu2">
								<center>
							    	<a class="btn btn-light" href="#" style="width:100%;">Buscar</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Ver todas</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Comparar</a>
						    	</center>
						  </div>
						</div>
				  	</li>
				 	<li class="nav-item">
				    	<div class="dropdown">
						  <button class="btn btn-light rounded-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Arriendos
						  </button>
						  <div class="dropdown-menu border border-secondary" aria-labelledby="dropdownMenu2">
								<center>
							    	<a class="btn btn-light" href="#" style="width:100%;">Buscar</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Ver todos</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Comparar</a>
						    	</center>
						  </div>
						</div>
				  	</li>
				  	<li class="nav-item">
				    	<div class="dropdown">
						  <button class="btn btn-light rounded-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Carreras
						  </button>
						  <div class="dropdown-menu border border-secondary" aria-labelledby="dropdownMenu2">
								<center>
							    	<a class="btn btn-light" href="#" style="width:100%;">Buscar</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Ver todas</a>
						    	</center>
						  </div>
						</div>
				  	</li>
				  	<li class="nav-item">
				    	<div class="dropdown">
						  <button class="btn btn-light rounded-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Lugares de entretencion
						  </button>
						  <div class="dropdown-menu border border-secondary" aria-labelledby="dropdownMenu2">
								<center>
							    	<a class="btn btn-light" href="#" style="width:100%;">Buscar</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Ver todas</a>
						    	</center>
						  </div>
						</div>
				  	</li>
				  	<li class="nav-item">
				    	<div class="dropdown">
						  <button class="btn btn-light rounded-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Becas
						  </button>
						  <div class="dropdown-menu border border-secondary" aria-labelledby="dropdownMenu2">
								<center>
							    	<a class="btn btn-light" href="#" style="width:100%;">Buscar</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Ver todas</a>
						    	</center>
						  </div>
						</div>
				  	</li>
				  	<li class="nav-item">
				    	<div class="dropdown">
						  <button class="btn btn-light rounded-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Eventos
						  </button>
						  <div class="dropdown-menu border border-secondary" aria-labelledby="dropdownMenu2">
								<center>
							    	<a class="btn btn-light" href="#" style="width:100%;">Buscar</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Ver todos</a>
						    	</center>
						  </div>
						</div>
				  	</li>
				</ul>
  			</div>
		</div>
	</div>
	<div class="container">
		<div class="row bg-dark">
			<div class="col-md-12">
				<div id="carousel-1" class="carousel slide" data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#carousel-1" data-slide-to="0" class="active"></li>
						<li data-target="#carousel-1" data-slide-to="1"></li>
						<li data-target="#carousel-1" data-slide-to="2"></li>
					</ol>
					<div class="carousel-inner" role="listbox">
					    <div class="carousel-item active">
					      <img id="IndexCar" src="../Img/index/sanSebastian.jpg" class="img-responsive" alt="">
					      <div class="carousel-caption">
					      	<h3 id="BgPortada" class="border border-dark border-bottom-0 rounded-0">Universidad San sebastian</h3>
					      	<p id="BgPortada" class="border border-dark border-top-0 rounded-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt sit praesentium facere laborum architecto aliquid ab quaerat eligendi doloremque odio porro perspiciatis ex in culpa quisquam odit, maiores, consectetur consequuntur.</p>
					      </div>
						</div>
						<div class="carousel-item">
					      <img id="IndexCar" src="../Img/index/sanSebastian.jpg" class="img-responsive" alt="">
					      <div class="carousel-caption">
					      	<h3 id="BgPortada"class="border border-dark border-bottom-0 rounded-0">Universidad AAAAAAAA</h3>
					      	<p id="BgPortada" class="border border-dark border-top-0 rounded-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt sit praesentium facere laborum architecto aliquid ab quaerat eligendi doloremque odio porro perspiciatis ex in culpa quisquam odit, maiores, consectetur consequuntur.</p>
					      </div>
						</div>
						<div class="carousel-item">
					      <img id="IndexCar" src="../Img/index/sanSebastian.jpg" class="img-responsive" alt="">
					      <div class="carousel-caption">
					      	<h3 id="BgPortada" class="border border-dark border-bottom-0 rounded-0">Universidad eeeeeeeeee</h3>
					      	<p id="BgPortada" class="border border-dark border-top-0 rounded-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt sit praesentium facere laborum architecto aliquid ab quaerat eligendi doloremque odio porro perspiciatis ex in culpa quisquam odit, maiores, consectetur consequuntur.</p>
					      </div>
						</div>
					</div>
					<a class="carousel-control-prev" href="#carousel-1" role="button" data-slide="prev">
					    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
					    <span class="sr-only">Anterior</span>
					  </a>
					  <a class="carousel-control-next" href="#carousel-1" role="button" data-slide="next">
					    <span class="carousel-control-next-icon" aria-hidden="true"></span>
					    <span class="sr-only">Siguiente</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div id="indexUni" class="col-md-4 border border-dark">
				<center>
					<br><br><br><br><br><br><br><br><br><br><br>
					<h2 class="border border-dark border-bottom-0 rounded-0" id="BgPortada">Universidades</h2>
					<p class="border border-dark border-top-0 rounded-0" id="BgPortada">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt sit praesentium facere laborum architecto aliquid ab quaerat eligendi porro perspiciatis ex in culpa.</p>
					<a href="#" class="btn btn-light border border-dark">Ver mas</a>
				</center>	
			</div>
			<div id="indexBeca" class="col-md-4 border border-dark">
				<center>
					<br><br><br><br><br><br><br><br><br><br><br>
					<h2 id="BgPortada" class="border border-dark border-bottom-0 rounded-0">Becas</h2>
					<p id="BgPortada" class="border border-dark border-top-0 rounded-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt sit praesentium facere laborum architecto aliquid ab quaerat eligendi porro perspiciatis ex in culpa.</p>
					<a href="#" class="btn btn-light border border-dark">Ver mas</a>
				</center>	
			</div>

			<div id="indexCarrera" class="col-md-4 border border-dark">
				<center>
					<br><br><br><br><br><br><br><br><br><br><br>
					<h2 class="border border-dark border-bottom-0 rounded-0" id="BgPortada">Carreras</h2>
					<p class="border border-dark border-top-0 rounded-0" id="BgPortada">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt sit praesentium facere laborum architecto aliquid ab quaerat eligendi porro perspiciatis ex in culpa.</p>
					<a href="#" class="btn btn-light border border-dark">Ver mas</a>
				</center>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div id="indexArriendo" class="col-md-4 border border-dark">
				<center>
					<br><br><br><br><br><br><br><br><br><br>
					<h2 class="border border-dark border-bottom-0 rounded-0" id="BgPortada2">Arriendos</h2>
					<p class="border border-dark border-top-0 rounded-0" id="BgPortada2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt sit praesentium facere laborum architecto aliquid ab quaerat eligendi porro perspiciatis ex in culpa.</p>
					<a href="#" class="btn btn-light border border-dark">Ver mas</a>
				</center>
			</div>

			<div id="indexLugar" class="col-md-4 border border-dark">
				<center>
					<br><br><br><br><br><br><br><br><br><br>
					<h3 class="border border-dark border-bottom-0 rounded-0" id="BgPortada2">Lugares de entretenimiento</h3>
					<p class="border border-dark border-top-0 rounded-0" id="BgPortada2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt sit praesentium facere laborum architecto aliquid ab quaerat eligendi porro perspiciatis ex in culpa.</p>
					<a href="#" class="btn btn-light border border-dark">Ver mas</a>
				</center>		
			</div>

			<div id="indexEvento" class="col-md-4 border border-dark">
				<center>
					<br><br><br><br><br><br><br><br><br><br>
					<h2 class="border border-dark border-bottom-0 rounded-0" id="BgPortada2">Eventos</h2>
					<p class="border border-dark border-top-0 rounded-0" id="BgPortada2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt sit praesentium facere laborum architecto aliquid ab quaerat eligendi porro perspiciatis ex in culpa.</p>
					<a href="#" class="btn btn-light border border-dark">Ver mas</a>
				</center>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div id="indexPuntuar" class="col-md-6 border border-dark">
				<center>
					<br><br><br><br><br><br><br><br><br>
					<h2 class="border border-dark border-bottom-0 rounded-0" id="BgPortada">Valora tu busqueda</h2>
					<p class="border border-dark border-top-0 rounded-0" id="BgPortada">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt sit praesentium facere laborum architecto aliquid ab quaerat eligendi porro perspiciatis ex in culpa.</p>
					<a href="#" class="btn btn-light border border-dark">Ver mas</a>
				</center>
			</div>
			<div id="indexComentar" class="col-md-6 border border-dark">
				<center>
					<br><br><br><br><br><br><br><br><br>
					<h3 class="border border-dark border-bottom-0 rounded-0" id="BgPortada">Comenta</h3>
					<p class="border border-dark border-top-0 rounded-0" id="BgPortada">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt sit praesentium facere laborum architecto aliquid ab quaerat eligendi porro perspiciatis ex in culpa.</p>
					<a href="#" class="btn btn-light border border-dark">Ver mas</a>
				</center>		
			</div>
		</div>
	</div>

	<div class="container-fluid bg-light">
  		<div class="row" >
  			<div class="col-md-4"></div>
	  			<div class="col-md-4" style="margin-left: 10px; margin-top: 20px;">
	  				<center>
		  				<h3 style="margin-top: 20px;">Dejanos tu sugerencia</h3>
		  				<p class="text-center" style="margin-top: 20px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus similique quidem rem ipsum ipsam a dolore.!</p>
		  				<a href="#" class="btn btn-light border border-dark" style="margin-bottom: 30px;">Hacer sugerencia</a>
	  				</center>
	  			</div>
	  		<div class="col-md-4"></div>
		</div>
	</div>
	<div class="container-fluid bg-dark">
  		<div class="row">
  			<div class="col-md-2"></div>
  			<div class="col-md-8">
	  			<center>
		  			<h3 class="text-white border border-white border-left-0 border-right-0 border-bottom-0" style="margin-top: 40px;">¿Te gusto la pagina?</h3>
		  			<h6 class="text-white">¡Compartela con tus amigos!</h6>
				<div class="btn-group dropright">
				  <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				 <img src="https://img.icons8.com/bubbles/50/000000/share.png">
				  </button>
				  <div class="dropdown-menu">
				    <a class="btn btn-light" href="#"><img src="https://img.icons8.com/bubbles/50/000000/facebook-new.png"></a>
							    <a class="btn btn-light" href="#"><img src="https://img.icons8.com/bubbles/50/000000/twitter.png"></a>
							    <a class="btn btn-light" href="#"><img src="https://img.icons8.com/bubbles/50/000000/google-plus.png"></a>
							    <a class="btn btn-light" href="#"><img src="https://img.icons8.com/bubbles/50/000000/instagram-new.png"></a>
							    <a class="btn btn-light" href="#"><img style="width: 50px;height: 50px;" src="https://img.icons8.com/dusk/64/000000/ms-outlook.png"></a>
				  </div>
				</div>
				<h6 class="text-white border border-white border-left-0 border-right-0 border-top-0" style="margin-bottom: 20px;">Compartir</h6>
				</center>
  			</div>
  			<div class="col-md-2"></div>
		</div>
		<center><p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit..</p>
		<li class="nav-item" style="margin-top: -35px;">
			<div class="dropdown">
				<button class="btn btn-transparent text-white" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Icons made by
				<div class="dropdown-menu" aria-labelledby="dropdownMenu2">
					<a class="btn btn-light" href="https://icons8.com/icon/118555/facebook-nuevo">Facebook Nuevo icon by Icons8</a>
					<a class="btn btn-light" href="https://icons8.com/icon/114209/google-plus">Google Plus icon by Icons8</a>
					<a class="btn btn-light" href="https://icons8.com/icon/108650/twitter">Twitter icon by Icons8></a>
					<a class="btn btn-light" href="https://icons8.com/icon/46977/ms-outlook">MS Outlook icon by Icons8</a>
					<a class="btn btn-light" href="https://icons8.com/icon/108646/instagram">Instagram icon by Icons8</a>
					<a class="btn btn-light" href="https://icons8.com/icon/109462/share">Share icon by Icons8</a>
					<a href="https://www.flaticon.es/autores/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.es/"             title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/"             title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a>
				</div>
			</div>
		</li>
		</center>
	</div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
