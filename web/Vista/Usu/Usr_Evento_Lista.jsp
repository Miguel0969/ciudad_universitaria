<%-- 
    Document   : Usr_Evento_Lista
    Created on : 11-oct-2019, 13:58:56
    Author     : Casa23
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
	<title>Carreras</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons.js"></script>
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    </head>
    <body class="bg-dark">
	<div class="container-fluid bg-dark">
  		<div class="row">
  			<div id="header" class="col-md-7" style=" margin-top: 20px;">
  				<center>
  					<a id="aCiuUni" class="navbar-brand text-white" href="#">Ciudad universitaria <br> Puerto Montt</a>
  				</center>
  			</div>
  			<div id="header" class="col-md-2 d-flex flex-row-reverse">
  				<h6 class="text-white">Nombre de usuario</h6>
  			</div>
  			<div class="col-md-3 d-flex flex-row">
  				<img style="height: 65px;width: 60px; margin-top: 10px;" src="../icons/estudiante.png">			
				<a href="#" id="btnCerrarSesion" class="btn btn-transparent"><img src="https://img.icons8.com/officel/26/000000/circled-right-2.png">Cerrar sesion</a>
  			</div>
		</div>
	</div>
	<!-- Cabecera2 --><!-- Cabecera2 --><!-- Cabecera2 --><!-- Cabecera2 --><!-- Cabecera2 --><!-- Cabecera2 --><!-- Cabecera2 --><!-- Cabecera2 --><!-- Cabecera2 --><!-- Cabecera2 --><!-- Cabecera2 --><!-- Cabecera2 --><!-- Cabecera2 --><!-- Cabecera2 --><!-- Cabecera2 --><!-- Cabecera2 -->
	<div class="container-fluid bg-light">
  		<div class="row border border-dark rounded-0 border-right-0 border-left-0">
  			<div class="col-md-12" style="margin-top: 10px;">
  				<ul class="nav justify-content-center">
  					<li class="nav-item">
  						<button class="btn btn-light rounded-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Inicio
						  </button>
  					</li>
				  	<li class="nav-item">
				    	<div class="dropdown">
						  <button class="btn btn-light rounded-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Universidades
						  </button>
						  <div class="dropdown-menu border border-secondary" aria-labelledby="dropdownMenu2">
								<center>
							    	<a class="btn btn-light" href="#" style="width:100%;">Buscar</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Ver todas</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Comparar</a>
						    	</center>
						  </div>
						</div>
				  	</li>
				 	<li class="nav-item">
				    	<div class="dropdown">
						  <button class="btn btn-light rounded-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Arriendos
						  </button>
						  <div class="dropdown-menu border border-secondary" aria-labelledby="dropdownMenu2">
								<center>
							    	<a class="btn btn-light" href="#" style="width:100%;">Buscar</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Ver todos</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Comparar</a>
						    	</center>
						  </div>
						</div>
				  	</li>
				  	<li class="nav-item">
				    	<div class="dropdown">
						  <button class="btn btn-light rounded-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Carreras
						  </button>
						  <div class="dropdown-menu border border-secondary" aria-labelledby="dropdownMenu2">
								<center>
							    	<a class="btn btn-light" href="#" style="width:100%;">Buscar</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Ver todas</a>
						    	</center>
						  </div>
						</div>
				  	</li>
				  	<li class="nav-item">
				    	<div class="dropdown">
						  <button class="btn btn-light rounded-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Lugares de entretencion
						  </button>
						  <div class="dropdown-menu border border-secondary" aria-labelledby="dropdownMenu2">
								<center>
							    	<a class="btn btn-light" href="#" style="width:100%;">Buscar</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Ver todas</a>
						    	</center>
						  </div>
						</div>
				  	</li>
				  	<li class="nav-item">
				    	<div class="dropdown">
						  <button class="btn btn-light rounded-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Becas
						  </button>
						  <div class="dropdown-menu border border-secondary" aria-labelledby="dropdownMenu2">
								<center>
							    	<a class="btn btn-light" href="#" style="width:100%;">Buscar</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Ver todas</a>
						    	</center>
						  </div>
						</div>
				  	</li>
				  	<li class="nav-item">
				    	<div class="dropdown">
						  <button class="btn btn-light rounded-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Eventos
						  </button>
						  <div class="dropdown-menu border border-secondary" aria-labelledby="dropdownMenu2">
								<center>
							    	<a class="btn btn-light" href="#" style="width:100%;">Buscar</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Ver todos</a>
						    	</center>
						  </div>
						</div>
				  	</li>
				</ul>
  			</div>
		</div>
	</div>
	<!-- PORTADA --><!-- PORTADA --><!-- PORTADA --><!-- PORTADA --><!-- PORTADA --><!-- PORTADA --><!-- PORTADA --><!-- PORTADA --><!-- PORTADA --><!-- PORTADA --><!-- PORTADA --><!-- PORTADA --><!-- PORTADA --><!-- PORTADA --><!-- PORTADA --><!-- PORTADA -->
	<div class="container">
		<div class="row bg-dark">
			<div class="col-md-12">
				<div id="carousel-1" class="carousel slide" data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#carousel-1" data-slide-to="0" class="active"></li>
						<li data-target="#carousel-1" data-slide-to="1"></li>
						<li data-target="#carousel-1" data-slide-to="2"></li>
					</ol>
					<div class="carousel-inner" role="listbox">
					    <div class="carousel-item active">
					      <img id="IndexCar" src="img/VerEventos/EventoPortada1.jpg" class="img-responsive" alt="">
					      <div class="carousel-caption">
					      	<h3 id="BgPortada" class="border border-dark border-bottom-0 rounded-0">Eventos y noticias</h3>
					      	<p id="BgPortada" class="border border-dark border-top-0 rounded-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt sit praesentium facere laborum architecto aliquid ab quaerat eligendi doloremque odio porro perspiciatis ex in culpa quisquam odit, maiores, consectetur consequuntur.</p>
					      </div>
						</div>
						<div class="carousel-item">
					      <img id="IndexCar" src="img/VerEventos/EventoPortada2.jpg" class="img-responsive" alt="">
					      <div class="carousel-caption">
					      	<h3 id="BgPortada"class="border border-dark border-bottom-0 rounded-0">Eventos y noticias</h3>
					      	<p id="BgPortada" class="border border-dark border-top-0 rounded-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt sit praesentium facere laborum architecto aliquid ab quaerat eligendi doloremque odio porro perspiciatis ex in culpa quisquam odit, maiores, consectetur consequuntur.</p>
					      </div>
						</div>
						<div class="carousel-item">
					      <img id="IndexCar" src="img/VerEventos/EventoPortada3.jpg" class="img-responsive" alt="">
					      <div class="carousel-caption">
					      	<h3 id="BgPortada" class="border border-dark border-bottom-0 rounded-0">Eventos y noticias</h3>
					      	<p id="BgPortada" class="border border-dark border-top-0 rounded-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt sit praesentium facere laborum architecto aliquid ab quaerat eligendi doloremque odio porro perspiciatis ex in culpa quisquam odit, maiores, consectetur consequuntur.</p>
					      </div>
						</div>
					</div>
					<a class="carousel-control-prev" href="#carousel-1" role="button" data-slide="prev">
					    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
					    <span class="sr-only">Anterior</span>
					  </a>
					  <a class="carousel-control-next" href="#carousel-1" role="button" data-slide="next">
					    <span class="carousel-control-next-icon" aria-hidden="true"></span>
					    <span class="sr-only">Siguiente</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo -->
	<div class="containe-fluid bg-light">
		<div class="row">
			<div class="col-md-8" id="casiFootArriendos">
				<center>
					<h2 style="margin-top: 30px;" class="border border-dark border-bottom-0 rounded-0"  id="BgPortada">Filtros</h2>
					<p  class="border border-dark border-top-0 rounded-0"  id="BgPortada">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id saepe laboriosam vitae rem totam, nulla sapiente sed.</p>		
				</center>
				<div class="dropdown" style="display: inline-block; margin-left: 30%;">
					<button class="btn btn-dark dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Universidades
					</button>
					<div class="dropdown-menu">
						<a class="btn btn-light" href="#">Universidad 1</a>
						<a class="btn btn-light" href="#">Universidad 2</a>
						<a class="btn btn-light" href="#">Universidad 3</a>
					</div>
				</div>
				<div class="dropdown" style="display: inline-block;">
					<button class="btn btn-dark dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Tipo de evento
					</button>
					<div class="dropdown-menu">
						<a class="btn btn-light" href="#">Evento 1</a>
						<a class="btn btn-light" href="#">Evento 2</a>
						<a class="btn btn-light" href="#">Evento 3</a>
					</div>
				</div>
				<a href="#" class="btn btn-dark">Buscar eventos</a>
				<br><br>
			</div>
		</div>
	</div>
	<!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo -->
	<div class="container-fluid">
		<div class="row bg-light">
			<div class="col-md-7">
				<div id="carousel-2" class="carousel slide" data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#carousel-2" data-slide-to="0" class="active"></li>
						<li data-target="#carousel-2" data-slide-to="1"></li>
						<li data-target="#carousel-2" data-slide-to="2"></li>
					</ol>
					<div class="carousel-inner" role="listbox">
					    <div class="carousel-item active">
					      <img id="IndexCar" src="img/VerEventos/Evento1.png" class="img-responsive" alt="">
						</div>
						<div class="carousel-item">
					      <img id="IndexCar" src="img/VerEventos/Evento2.png" class="img-responsive" alt="">
						</div>
						<div class="carousel-item">
					      <img id="IndexCar" src="img/VerEventos/Evento3.png" class="img-responsive" alt="">
						</div>
					</div>

					<a class="carousel-control-prev" href="#carousel-2" role="button" data-slide="prev">
					    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
					    <span class="sr-only">Anterior</span>
					  </a>
					  <a class="carousel-control-next" href="#carousel-2" role="button" data-slide="next">
					    <span class="carousel-control-next-icon" aria-hidden="true"></span>
					    <span class="sr-only">Siguiente</span>
					</a>
				</div>
			</div>
			<div class="col-md-5 border border-dark">
				<br><br><br><br><br>
				<center>
					<h2 class="border border-dark border-bottom-0 rounded-0">Evento 1</h2>
					<br>
					<p class="border border-dark border-top-0 rounded-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam quia accusantium et sequi ipsa dolor, deserunt. Minima ipsum porro doloremque mollitia laudantium deleniti itaque repellendus aperiam, harum facilis id laboriosam?</p>
					<h6><a href="#" class="btn btn-dark">Ver mas</a> <div class="btn-group dropright">
				  <button class="btn btn-dark dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				 	<img id="ArriendoCompar2" src="https://img.icons8.com/bubbles/50/000000/share.png">
				  </button>
				  <div class="dropdown-menu">
				    	<a class="btn btn-light" href="#"><img id="ArriendoCompar" src="https://img.icons8.com/bubbles/50/000000/facebook-new.png"></a>
						<a class="btn btn-light" href="#"><img id="ArriendoCompar" src="https://img.icons8.com/bubbles/50/000000/twitter.png"></a>
						<a class="btn btn-light" href="#"><img id="ArriendoCompar" src="https://img.icons8.com/bubbles/50/000000/google-plus.png"></a>
						<a class="btn btn-light" href="#"><img id="ArriendoCompar" src="https://img.icons8.com/bubbles/50/000000/instagram-new.png"></a>
						<a class="btn btn-light" href="#"><img id="ArriendoCompar" src="https://img.icons8.com/dusk/64/000000/ms-outlook.png"></a>
				  </div>
				</div></h6>
				</center>
			</div>	
		</div>
	</div>
	<!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo -->
	<div class="container-fluid">
		<div class="row bg-light">
			<div class="col-md-7">
				<div id="carousel-3" class="carousel slide" data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#carousel-3" data-slide-to="0" class="active"></li>
						<li data-target="#carousel-3" data-slide-to="1"></li>
						<li data-target="#carousel-3" data-slide-to="2"></li>
					</ol>
					<div class="carousel-inner" role="listbox">
					    <div class="carousel-item active">
					      <img id="IndexCar" src="img/VerEventos/Evento1.png" class="img-responsive" alt="">
						</div>
						<div class="carousel-item">
					      <img id="IndexCar" src="img/VerEventos/Evento2.png" class="img-responsive" alt="">
						</div>
						<div class="carousel-item">
					      <img id="IndexCar" src="img/VerEventos/Evento3.png" class="img-responsive" alt="">
						</div>
					</div>

					<a class="carousel-control-prev" href="#carousel-3" role="button" data-slide="prev">
					    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
					    <span class="sr-only">Anterior</span>
					  </a>
					  <a class="carousel-control-next" href="#carousel-3" role="button" data-slide="next">
					    <span class="carousel-control-next-icon" aria-hidden="true"></span>
					    <span class="sr-only">Siguiente</span>
					</a>
				</div>
			</div>
			<div class="col-md-5 border border-dark">
				<br><br><br><br><br>
				<center>
					<h2 class="border border-dark border-bottom-0 rounded-0">Evento 2</h2>
					<br>
					<p class="border border-dark border-top-0 rounded-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam quia accusantium et sequi ipsa dolor, deserunt. Minima ipsum porro doloremque mollitia laudantium deleniti itaque repellendus aperiam, harum facilis id laboriosam?</p>
					<h6><a href="#" class="btn btn-dark">Ver mas</a>  <div class="btn-group dropright">
				  <button class="btn btn-dark dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				 	<img id="ArriendoCompar2" src="https://img.icons8.com/bubbles/50/000000/share.png">
				  </button>
				  <div class="dropdown-menu">
				    	<a class="btn btn-light" href="#"><img id="ArriendoCompar" src="https://img.icons8.com/bubbles/50/000000/facebook-new.png"></a>
						<a class="btn btn-light" href="#"><img id="ArriendoCompar" src="https://img.icons8.com/bubbles/50/000000/twitter.png"></a>
						<a class="btn btn-light" href="#"><img id="ArriendoCompar" src="https://img.icons8.com/bubbles/50/000000/google-plus.png"></a>
						<a class="btn btn-light" href="#"><img id="ArriendoCompar" src="https://img.icons8.com/bubbles/50/000000/instagram-new.png"></a>
						<a class="btn btn-light" href="#"><img id="ArriendoCompar" src="https://img.icons8.com/dusk/64/000000/ms-outlook.png"></a>
				  </div>
				</div></h6>
				</center>
			</div>	
		</div>
	</div>
	<!-- paginacion --><!-- paginacion --><!-- paginacion --><!-- paginacion --><!-- paginacion --><!-- paginacion --><!-- paginacion --><!-- paginacion --><!-- paginacion -->
	<div class="containe-fluid bg-light">
		<div class="row border border-dark">
			<div class="col-md-8 border border-top-0 border-bottom-0" id="casiFootArriendos">
				<center>
					<br><br>
					<nav aria-label="Page navigation example">
					  <ul class="pagination">
					    <li class="page-item" id="PaginacionArriendo1">
					      <a class="page-link btn btn-dark" href="#" aria-label="Previous">
					        <span aria-hidden="true">&laquo;</span>
					        <span class="sr-only">Previous</span>
					      </a>
					    </li>
					    <li class="page-item"><a class="page-link btn" href="#">1</a></li>
					    <li class="page-item"><a class="page-link btn " href="#">2</a></li>
					    <li class="page-item"><a class="page-link btn" href="#">3</a></li>
					    <li class="page-item" id="PaginacionArriendo2">
					      <a class="page-link btn" href="#" aria-label="Next">
					        <span aria-hidden="true">&raquo;</span>
					        <span class="sr-only">Next</span>
					      </a>
					    </li>
					  </ul>
					</nav>
					<br>
				</center>
			</div>
		</div>
	</div>
	<!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer -->
	<!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer -->
	<div class="container-fluid bg-dark">
  		<div class="row">
  			<div class="col-md-2"></div>
  			<div class="col-md-8">
	  			<center>
		  			<h3 class="text-white border border-white border-left-0 border-right-0 border-bottom-0" style="margin-top: 40px;">¿Te gusto la pagina?</h3>
		  			<h6 class="text-white">¡Compartela con tus amigos!</h6>
				<div class="btn-group dropright">
				  <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				 	<img src="https://img.icons8.com/bubbles/50/000000/share.png">
				  </button>
				  <div class="dropdown-menu">
				    <a class="btn btn-light" href="#"><img src="https://img.icons8.com/bubbles/50/000000/facebook-new.png"></a>
							    <a class="btn btn-light" href="#"><img src="https://img.icons8.com/bubbles/50/000000/twitter.png"></a>
							    <a class="btn btn-light" href="#"><img src="https://img.icons8.com/bubbles/50/000000/google-plus.png"></a>
							    <a class="btn btn-light" href="#"><img src="https://img.icons8.com/bubbles/50/000000/instagram-new.png"></a>
							    <a class="btn btn-light" href="#"><img style="width: 50px;height: 50px;" src="https://img.icons8.com/dusk/64/000000/ms-outlook.png"></a>
				  </div>
				</div>
				<h6 class="text-white border border-white border-left-0 border-right-0 border-top-0" style="margin-bottom: 20px;">Compartir</h6>
				</center>
  			</div>
  			<div class="col-md-2"></div>
		</div>
		<center><p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit..</p>
		<li class="nav-item" style="margin-top: -35px;">
			<div class="dropdown">
				<button class="btn btn-transparent text-white" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Icons by
				</button>
				<div class="dropdown-menu" aria-labelledby="dropdownMenu2">
					<a class="btn btn-light" href="https://icons8.com/icon/118555/facebook-nuevo">Facebook Nuevo icon by Icons8</a>
					<a class="btn btn-light" href="https://icons8.com/icon/114209/google-plus">Google Plus icon by Icons8</a>
					<a class="btn btn-light" href="https://icons8.com/icon/108650/twitter">Twitter icon by Icons8></a>
					<a class="btn btn-light" href="https://icons8.com/icon/46977/ms-outlook">MS Outlook icon by Icons8</a>
					<a class="btn btn-light" href="https://icons8.com/icon/108646/instagram">Instagram icon by Icons8</a>
					<a class="btn btn-light" href="https://icons8.com/icon/109462/share">Share icon by Icons8</a>
					<a href="https://www.flaticon.es/autores/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.es/"             title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/"             title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a>
					<a href="https://icons8.com/icon/4N5w6YH6O7Ub/derecha-en-círculo-2">Derecha en círculo 2 icon by Icons8</a>
				</div>
			</div>
		</li>
		</center>
	</div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
