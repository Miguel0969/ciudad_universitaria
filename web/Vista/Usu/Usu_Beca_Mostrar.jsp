<%-- 
    Document   : Usu_Beca_Mostrar
    Created on : 11-oct-2019, 13:52:05
    Author     : Casa23
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
	<title>Beca</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons.js"></script>
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    </head>
    <body class="bg-dark">
	<!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera -->
	<div class="container-fluid bg-dark">
  		<div class="row">
  			<div id="header" class="col-md-7" style=" margin-top: 20px;">
  				<center>
  					<a id="aCiuUni" class="navbar-brand text-white" href="#">Ciudad universitaria <br> Puerto Montt</a>
  				</center>
  			</div>
  			<div id="header" class="col-md-2 d-flex flex-row-reverse">
  				<h6 class="text-white">Nombre de usuario</h6>
  			</div>
  			<div class="col-md-3 d-flex flex-row">
  				<img style="height: 65px;width: 60px; margin-top: 10px;" src="../icons/estudiante.png">			
				<a href="#" id="btnCerrarSesion" class="btn btn-transparent"><img src="https://img.icons8.com/officel/26/000000/circled-right-2.png">Cerrar sesion</a>
  			</div>
		</div>
	</div>
	<!-- Cabecera2 --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera2 -->
	<div class="container-fluid bg-light">
  		<div class="row border border-dark rounded-0 border-right-0 border-left-0">
  			<div class="col-md-12" style="margin-top: 10px;">
  				<ul class="nav justify-content-center">
  					<li class="nav-item">
  						<button class="btn btn-light rounded-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Inicio
						  </button>
  					</li>
				  	<li class="nav-item">
				    	<div class="dropdown">
						  <button class="btn btn-light rounded-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Universidades
						  </button>
						  <div class="dropdown-menu border border-secondary" aria-labelledby="dropdownMenu2">
								<center>
							    	<a class="btn btn-light" href="#" style="width:100%;">Buscar</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Ver todas</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Comparar</a>
						    	</center>
						  </div>
						</div>
				  	</li>
				 	<li class="nav-item">
				    	<div class="dropdown">
						  <button class="btn btn-light rounded-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Arriendos
						  </button>
						  <div class="dropdown-menu border border-secondary" aria-labelledby="dropdownMenu2">
								<center>
							    	<a class="btn btn-light" href="#" style="width:100%;">Buscar</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Ver todos</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Comparar</a>
						    	</center>
						  </div>
						</div>
				  	</li>
				  	<li class="nav-item">
				    	<div class="dropdown">
						  <button class="btn btn-light rounded-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Carreras
						  </button>
						  <div class="dropdown-menu border border-secondary" aria-labelledby="dropdownMenu2">
								<center>
							    	<a class="btn btn-light" href="#" style="width:100%;">Buscar</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Ver todas</a>
						    	</center>
						  </div>
						</div>
				  	</li>
				  	<li class="nav-item">
				    	<div class="dropdown">
						  <button class="btn btn-light rounded-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Lugares de entretencion
						  </button>
						  <div class="dropdown-menu border border-secondary" aria-labelledby="dropdownMenu2">
								<center>
							    	<a class="btn btn-light" href="#" style="width:100%;">Buscar</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Ver todas</a>
						    	</center>
						  </div>
						</div>
				  	</li>
				  	<li class="nav-item">
				    	<div class="dropdown">
						  <button class="btn btn-light rounded-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Becas
						  </button>
						  <div class="dropdown-menu border border-secondary" aria-labelledby="dropdownMenu2">
								<center>
							    	<a class="btn btn-light" href="#" style="width:100%;">Buscar</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Ver todas</a>
						    	</center>
						  </div>
						</div>
				  	</li>
				  	<li class="nav-item">
				    	<div class="dropdown">
						  <button class="btn btn-light rounded-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Eventos
						  </button>
						  <div class="dropdown-menu border border-secondary" aria-labelledby="dropdownMenu2">
								<center>
							    	<a class="btn btn-light" href="#" style="width:100%;">Buscar</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Ver todos</a>
						    	</center>
						  </div>
						</div>
				  	</li>
				</ul>
  			</div>
		</div>
	</div>
	<!-- PORTADA --><!-- PORTADA --><!-- PORTADA --><!-- PORTADA --><!-- PORTADA --><!-- PORTADA --><!-- PORTADA --><!-- PORTADA --><!-- PORTADA --><!-- PORTADA --><!-- PORTADA --><!-- PORTADA --><!-- PORTADA --><!-- PORTADA --><!-- PORTADA --><!-- PORTADA -->
	<div class="container">
		<div class="row bg-dark">
			<div class="col-md-12" id="verBeca">
				<center>
					<h2 class="border border-dark border-bottom-0 rounded-0" id="txtVU" style="margin-top: 10%;">Beca 1</h2>
					<p class="border border-dark border-top-0 rounded-0" id="txtVU">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt, molestiae! Quas possimus pariatur autem molestiae. Expedita dolorem recusandae exercitationem nulla saepe ullam modi, quidem itaque, tempore porro corporis mollitia facere.</p>
					<div class="dropdown" id="txtVU2">
						<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 10px;">
					 	Compartir
					  	</button>
					  	<div class="dropdown-menu bg-transparent">
					    	<a class="btn btn-secondary" href="#"><img id="ArriendoCompar" src="https://img.icons8.com/bubbles/50/000000/facebook-new.png"></a>
							<a class="btn btn-secondary" href="#"><img id="ArriendoCompar" src="https://img.icons8.com/bubbles/50/000000/twitter.png"></a>
							<a class="btn btn-secondary" href="#"><img id="ArriendoCompar" src="https://img.icons8.com/bubbles/50/000000/google-plus.png"></a>
							<a class="btn btn-secondary" href="#"><img id="ArriendoCompar" src="https://img.icons8.com/bubbles/50/000000/instagram-new.png"></a>
							<a class="btn btn-secondary" href="#"><img id="ArriendoCompar" src="https://img.icons8.com/dusk/64/000000/ms-outlook.png"></a>
					 	</div>
					</div>
				</center> 
			</div>
		</div>
	</div>
	<div class="container bg-light">
		<div class="row">
			<div class="col-md-8" id="casiFootArriendos">
				<center>
					<br><br>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi architecto animi officiis voluptas, distinctio debitis corporis earum, odit nulla quia temporibus velit maxime neque veritatis quis voluptates. Praesentium, nisi. Vero!
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vel vitae nobis architecto id, sint laborum eveniet ea odit distinctio, delectus sed debitis. Neque, quos, deserunt! Culpa atque at aperiam a.
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic sit placeat, harum laudantium asperiores magnam, dignissimos natus soluta modi velit omnis perferendis odio incidunt dolorem, quia architecto voluptates ex veniam!
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum et veniam, incidunt ipsam. Adipisci temporibus ad quaerat blanditiis, ex mollitia consequatur voluptatibus distinctio, doloremque, repudiandae eius reiciendis tempora dignissimos libero.</p>
					<br>
				</center>
			</div>
		</div>
	</div>
	<!-- Comentarios --><!-- Comentarios --><!-- Comentarios --><!-- Comentarios --><!-- Comentarios --><!-- Comentarios --><!-- Comentarios --><!-- Comentarios -->
	<!-- Comentarios --><!-- Comentarios --><!-- Comentarios --><!-- Comentarios --><!-- Comentarios -->
		<div class="container-fluid bg-light border border-dark border-left-0 border-right-0 border-bottom-0">
			<div class="row">
				<div class="col-md-8" id="casiFootArriendos">
					<center>
						<br>
						<h2>Comentarios</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perspiciatis culpa laboriosam reprehenderit quidem molestias!.</p>
					</center>
				</div>
			</div>
			<div class="row border border-dark border-right-0 border-left-0 border-top-0" >
				<br>
				<div class="col-md-8 border" id="casiFootArriendos" style="margin-bottom: 10px;">
					<br>
					<center>
						<textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
						<br>
						<a href="" class="btn btn-info">Publicar comentario</a>
						<br><br>
					</center>
					<br>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8 border border-dark border-left-0 border-right-0" id="casiFootArriendos" style="margin-top: 10px; margin-bottom: 10px;">
					<center>
						<a href="" class="btn btn-transparent">Ocultar comentarios</a>
					</center> 
				</div>
			</div>
			<div class="row" id="obj1">
				<div class="col-md-8 border border-dark rounded" id="casiFootArriendos" style="margin-top: 10px; margin-bottom: 15px;">
					<center>
						<br>
						<h2 class="border border-right-0 border-left-0 border-top-0">Nombre usuario</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati amet eos officia quam fugiat, debitis eveniet impedit.
						Et autem deserunt necessitatibus perspiciatis fugit voluptates. Quasi odio fugiat, inventore voluptas molestiae.</p>
						<i class="fas fa-star text-info" id="estrellas">★</i>
						<i class="fas fa-star text-info" id="estrellas">☆</i>
						<i class="far fa-star text-info" id="estrellas">☆</i>
						<i class="far fa-star text-info" id="estrellas">☆</i>
						<i class="far fa-star text-info" id="estrellas">☆</i>
					</center>
				</div>
			</div>
		</div>

	<!-- FOOOTer2222 --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer -->
	<!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer -->
	<div class="container-fluid bg-dark">
  		<div class="row">
  			<div class="col-md-2"></div>
  			<div class="col-md-8">
	  			<center>
		  			<h3 class="text-white border border-white border-left-0 border-right-0 border-bottom-0" style="margin-top: 40px;">¿Te gusto la pagina?</h3>
		  			<h6 class="text-white">¡Compartela con tus amigos!</h6>
				<div class="btn-group dropright">
				  <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				 	<img src="https://img.icons8.com/bubbles/50/000000/share.png">
				  </button>
				  <div class="dropdown-menu">
				    <a class="btn btn-light" href="#"><img src="https://img.icons8.com/bubbles/50/000000/facebook-new.png"></a>
							    <a class="btn btn-light" href="#"><img src="https://img.icons8.com/bubbles/50/000000/twitter.png"></a>
							    <a class="btn btn-light" href="#"><img src="https://img.icons8.com/bubbles/50/000000/google-plus.png"></a>
							    <a class="btn btn-light" href="#"><img src="https://img.icons8.com/bubbles/50/000000/instagram-new.png"></a>
							    <a class="btn btn-light" href="#"><img style="width: 50px;height: 50px;" src="https://img.icons8.com/dusk/64/000000/ms-outlook.png"></a>
				  </div>
				</div>
				<h6 class="text-white border border-white border-left-0 border-right-0 border-top-0" style="margin-bottom: 20px;">Compartir</h6>
				</center>
  			</div>
  			<div class="col-md-2"></div>
		</div>
		<center><p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit..</p>
		<li class="nav-item" style="margin-top: -35px;">
			<div class="dropdown">
				<button class="btn btn-transparent text-white" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Icons by
				</button>
				<div class="dropdown-menu" aria-labelledby="dropdownMenu2">
					<a class="btn btn-light" href="https://icons8.com/icon/118555/facebook-nuevo">Facebook Nuevo icon by Icons8</a>
					<a class="btn btn-light" href="https://icons8.com/icon/114209/google-plus">Google Plus icon by Icons8</a>
					<a class="btn btn-light" href="https://icons8.com/icon/108650/twitter">Twitter icon by Icons8></a>
					<a class="btn btn-light" href="https://icons8.com/icon/46977/ms-outlook">MS Outlook icon by Icons8</a>
					<a class="btn btn-light" href="https://icons8.com/icon/108646/instagram">Instagram icon by Icons8</a>
					<a class="btn btn-light" href="https://icons8.com/icon/109462/share">Share icon by Icons8</a>
					<a href="https://www.flaticon.es/autores/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.es/"             title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/"             title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a>
					<a href="https://icons8.com/icon/4N5w6YH6O7Ub/derecha-en-círculo-2">Derecha en círculo 2 icon by Icons8</a>
				</div>
			</div>
		</li>
		</center>
	</div>

	<!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
