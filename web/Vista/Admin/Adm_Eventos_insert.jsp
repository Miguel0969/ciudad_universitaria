<%-- 
    Document   : Adm_Evento_insert
    Created on : 11-oct-2019, 13:34:22
    Author     : Casa23
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
	<title>Agregar Evento</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons.js"></script>
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    </head>
    <body class="bg-dark">
	<!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera -->
	<div class="container-fluid bg-dark">
  		<div class="row">
  			<div id="header" class="col-md-7" style=" margin-top: 20px;">
  				<center>
  					<a id="aCiuUni" class="navbar-brand text-white" href="#">Ciudad universitaria <br> Puerto Montt</a>
  				</center>
  			</div>
  			<div id="header" class="col-md-2 d-flex flex-row-reverse">
  				<h6 class="text-white">Nombre de usuario</h6>
  			</div>
  			<div class="col-md-3 d-flex flex-row">
  				<img style="height: 65px;width: 60px; margin-top: 10px;" src="iconos/estudiante.png">			
				<a href="#" id="btnCerrarSesion" class="btn btn-transparent"><img src="https://img.icons8.com/officel/26/000000/circled-right-2.png">Cerrar sesion</a>
  			</div>
		</div>
	</div>
	<!-- Cabecera2 --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera --><!-- Cabecera -->
	<div class="container-fluid bg-light">
  		<div class="row border border-dark rounded-0 border-right-0 border-left-0">
  			<div class="col-md-12" style="margin-top: 10px;">
  				<ul class="nav justify-content-center">
  					<li class="nav-item">
  						<button class="btn btn-light rounded-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Inicio
						  </button>
  					</li>
				  	<li class="nav-item">
				    	<div class="dropdown">
						  <button class="btn btn-light rounded-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Universidades
						  </button>
						  <div class="dropdown-menu border border-secondary" aria-labelledby="dropdownMenu2">
								<center>
							    	<a class="btn btn-light" href="#" style="width:100%;">Buscar</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Ver todas</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Comparar</a>
						    	</center>
						  </div>
						</div>
				  	</li>
				 	<li class="nav-item">
				    	<div class="dropdown">
						  <button class="btn btn-light rounded-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Arriendos
						  </button>
						  <div class="dropdown-menu border border-secondary" aria-labelledby="dropdownMenu2">
								<center>
							    	<a class="btn btn-light" href="#" style="width:100%;">Buscar</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Ver todos</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Comparar</a>
						    	</center>
						  </div>
						</div>
				  	</li>
				  	<li class="nav-item">
				    	<div class="dropdown">
						  <button class="btn btn-light rounded-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Carreras
						  </button>
						  <div class="dropdown-menu border border-secondary" aria-labelledby="dropdownMenu2">
								<center>
							    	<a class="btn btn-light" href="#" style="width:100%;">Buscar</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Ver todas</a>
						    	</center>
						  </div>
						</div>
				  	</li>
				  	<li class="nav-item">
				    	<div class="dropdown">
						  <button class="btn btn-light rounded-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Lugares de entretencion
						  </button>
						  <div class="dropdown-menu border border-secondary" aria-labelledby="dropdownMenu2">
								<center>
							    	<a class="btn btn-light" href="#" style="width:100%;">Buscar</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Ver todas</a>
						    	</center>
						  </div>
						</div>
				  	</li>
				  	<li class="nav-item">
				    	<div class="dropdown">
						  <button class="btn btn-light rounded-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Becas
						  </button>
						  <div class="dropdown-menu border border-secondary" aria-labelledby="dropdownMenu2">
								<center>
							    	<a class="btn btn-light" href="#" style="width:100%;">Buscar</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Ver todas</a>
						    	</center>
						  </div>
						</div>
				  	</li>
				  	<li class="nav-item">
				    	<div class="dropdown">
						  <button class="btn btn-light rounded-0" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Eventos
						  </button>
						  <div class="dropdown-menu border border-secondary" aria-labelledby="dropdownMenu2">
								<center>
							    	<a class="btn btn-light" href="#" style="width:100%;">Buscar</a>
							    	<a class="btn btn-light" href="#" style="width:100%;">Ver todos</a>
						    	</center>
						  </div>
						</div>
				  	</li>
				</ul>
  			</div>
		</div>
	</div>
	<!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo --><!-- cuerpo -->
	<div class="container bg-light">
		<div class="row">
			<div class="col-md-8" id="casiFootArriendos">
				<center>
					<br>
					<h2>Agregar evento</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis quo, libero omnis nesciunt cumque asperiores consectetur. </p>
					<br>
				</center>
			</div>
		</div>
		<div class="row">
			<div class="col-md-5" id="PaginacionArriendo1">
				<center>
					<h6>Nombre de evento</h6>
					<input class="form-control" type="text" placeholder="Nombre">
					<br>
				</center>
			</div>
			<div class="col-md-3" >
				<center>
					<div class="form-group">
					    <label for="inputState">Universidad (opcional)</label>
					    <select id="inputState" class="form-control">
					        <option selected>Seleccione...</option>
					        <option>xxxxx</option>
					        <option>xxxxx</option>
					        <option>xxxxx</option>
					    </select>
				    </div>
				</center>
			</div>
			<div class="col-md-4" id="PaginacionArriendo2">
				<center>
					<center>
					<h6>Tipo de evento</h6>
					<input class="form-control" type="text" placeholder="Nombre">
					<br>
				</center>
				</center>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8" id="casiFootArriendos">
				<center>
					<h6>Ubicacion de evento (link google maps)</h6>
					<input class="form-control" type="text" placeholder="Link">
					<br>
					<h6>Descripcion de lugar (calle, numero de calle, etc.)</h6>
					<input class="form-control" type="text" placeholder="Ejemplo: CALLE AGUSTIN LARA NO. 69-B">
					<br>
				</center>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8" id="casiFootArriendos">
				<table class="table">
				  <thead id="tbFacul">
				    <tr>
				      <th scope="col" id="tbBscArriendo">Dia</th>
				      <th scope="col" id="tbBscArriendo">Comienzo</th>
				      <th scope="col" id="tbBscArriendo">Termino</th>
				    </tr>
				  </thead>
				  <tbody>
				    <tr>
					    <td id="tbBscArriendo">Lunes</td>
					    <td id="tbBscArriendo"><input class="form-control" type="text" placeholder="Cerrado"></td>
					    <td id="tbBscArriendo"><input class="form-control" type="text" placeholder="Cerrado"><td>
				    </tr>
					<tr>
					    <td id="tbBscArriendo">Martes</td>
					    <td id="tbBscArriendo"><input class="form-control" type="text" placeholder="Cerrado"></td>
					    <td id="tbBscArriendo"><input class="form-control" type="text" placeholder="Cerrado"><td>
				    </tr>
				    <tr>
					    <td id="tbBscArriendo">Miercoles</td>
					    <td id="tbBscArriendo"><input class="form-control" type="text" placeholder="Cerrado"></td>
					    <td id="tbBscArriendo"><input class="form-control" type="text" placeholder="Cerrado"><td>
				    </tr>
				    <tr>
					    <td id="tbBscArriendo">Jueves</td>
					    <td id="tbBscArriendo"><input class="form-control" type="text" placeholder="Cerrado"></td>
					    <td id="tbBscArriendo"><input class="form-control" type="text" placeholder="Cerrado"></td>
				    </tr>
				     <tr>
					    <td id="tbBscArriendo">Viernes</td>
					    <td id="tbBscArriendo"><input class="form-control" type="text" placeholder="Cerrado"></td>
					    <td id="tbBscArriendo"><input class="form-control" type="text" placeholder="Cerrado"><td>
				    </tr>
				    <tr>
					    <td id="tbBscArriendo">Sabado</td>
					    <td id="tbBscArriendo"><input class="form-control" type="text" placeholder="Cerrado"></td>
					    <td id="tbBscArriendo"><input class="form-control" type="text" placeholder="Cerrado"><td>
				    </tr>
				    <tr>
					    <td id="tbBscArriendo">Domingo</td>
					    <td id="tbBscArriendo"><input class="form-control" type="text" placeholder="Cerrado"></td>
					    <td id="tbBscArriendo"><input class="form-control" type="text" placeholder="Cerrado"><td>
				    </tr>
				  </tbody>
				</table>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-7">
				<center>
					<br>
					<h6>Agregar descripcion de evento</h6>
					<textarea class="form-control" id="exampleFormControlTextarea1" rows="7"></textarea>
					<br>
				</center>
			</div>
			<div class="col-md-5">
				<form>
					<br><br>
				  <div class="form-group">
				    <label for="exampleFormControlFile1">Agregar imagen</label>
				    <input type="file" class="form-control-file" id="exampleFormControlFile1">
				  </div>
				</form>			
			</div>
		</div>
		<div class="row">
			<div class="col-md-8" id="casiFootArriendos">
				<center>
					<a href="" class="btn btn-secondary" style="margin-bottom: 30px;">Agregar evento</a> 
				</center>
			</div>
		</div>
	</div>

	<!-- CabeceraInf --><!-- CabeceraInf --><!-- CabeceraInf --><!-- CabeceraInf --><!-- CabeceraInf -->
	<!-- CabeceraInf --><!-- CabeceraInf --><!-- CabeceraInf -->
	<!-- CabeceraInf --><!-- CabeceraInf --><!-- CabeceraInf --><!-- CabeceraInf -->
	<div class="container-fluid bg-info">
  		<div class="row border border-dark rounded-0 border-right-0 border-left-0">
  			<div class="col-md-12">
  				<center>
  					<br>
  					<h2 class="text-white">Administración</h2>
  					<br>
  				</center>
  				<ul class="nav justify-content-center bg-danger">
				  	<button class="btn bg-danger border border-white text-white border-left-0 border-right-0 rounded-0" type="button" href="#">
						Universidad  
					</button>
  					<button class="btn bg-danger border border-white text-white border-left-0 border-right-0 rounded-0" type="button" href="#">
						Eventos
					</button>
				  	<button class="btn bg-danger border border-white text-white border-left-0 border-right-0 rounded-0" type="button" href="#">
						Arriendos
					</button>
					<button class="btn bg-danger border border-white text-white border-left-0 border-right-0 rounded-0" type="button" href="#">
						Datos  
					</button>
					<button class="btn bg-danger border border-white text-white border-left-0 border-right-0 rounded-0" type="button" href="#">
						Carreras 
					</button>
					<button class="btn bg-danger border border-white text-white border-left-0 border-right-0 rounded-0" type="button" href="#">
						Lugares de entretencion
					</button>
					<button class="btn bg-danger border border-white text-white border-left-0 border-right-0 rounded-0" type="button" href="#">
						Becas
					</button>
					<button class="btn bg-danger border border-white text-white border-left-0 border-right-0 rounded-0" type="button" href="#">
						Sugerencias
					</button>
					<button class="btn bg-danger border border-white text-white border-left-0 border-right-0 rounded-0" type="button" href="#">
						Usuarios
					</button>
				</ul>
				<br>
  			</div>
		</div>
	</div>	
	<!-- FOOOTer2222 --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer -->
	<!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer --><!-- FOOOTer -->
	<div class="container-fluid bg-dark">
		<center><p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit..</p>
		<li class="nav-item" style="margin-top: -35px;">
			<div class="dropdown">
				<button class="btn btn-transparent text-white" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Icons by
				</button>
				<div class="dropdown-menu" aria-labelledby="dropdownMenu2">
					<a class="btn btn-light" href="https://icons8.com/icon/118555/facebook-nuevo">Facebook Nuevo icon by Icons8</a>
					<a class="btn btn-light" href="https://icons8.com/icon/114209/google-plus">Google Plus icon by Icons8</a>
					<a class="btn btn-light" href="https://icons8.com/icon/108650/twitter">Twitter icon by Icons8></a>
					<a class="btn btn-light" href="https://icons8.com/icon/46977/ms-outlook">MS Outlook icon by Icons8</a>
					<a class="btn btn-light" href="https://icons8.com/icon/108646/instagram">Instagram icon by Icons8</a>
					<a class="btn btn-light" href="https://icons8.com/icon/109462/share">Share icon by Icons8</a>
					<a href="https://www.flaticon.es/autores/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.es/"             title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/"             title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a>
					<a href="https://icons8.com/icon/4N5w6YH6O7Ub/derecha-en-círculo-2">Derecha en círculo 2 icon by Icons8</a>
				</div>
			</div>
		</li>
		</center>
	</div>
	<!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts --><!-- Scripts -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
